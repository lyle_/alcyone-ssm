BINDIR := bin
OBJDIR := obj
TESTDIR := tests
VPATH = bin src include
CFLAGS = -Wall -g -I include -I libs/iniparser/src
LDLIBS = -lncurses
CHECKLIBS = $(shell pkg-config --cflags --libs check)

.PHONY: all check clean
all: compile_commands.json directories ecudump ecuscan simulate_ecuscan

bear_path := $(shell which bear 2> /dev/null)
compile_commands.json:
    ifdef bear_path
        ifeq ($(MAKELEVEL), 0)
			$(info *** Running 'bear make' to create a compilation database for linters)
			@bear $(MAKE)
        endif
    else
		$(warning *** No 'bear' found in $$PATH, consider installing it to create a compilation database for linters)
    endif

directories: | $(OBJDIR) $(BINDIR) libs/iniparser/Makefile
$(BINDIR):
	mkdir $(BINDIR)
$(OBJDIR):
	mkdir $(OBJDIR)
libs/iniparser/Makefile:
	mkdir -p libs/iniparser
	git submodule update --init
clean:
	rm -fr compile_commands.json $(BINDIR) $(OBJDIR)

$(OBJDIR)/%.o: %.c
	$(COMPILE.c) $(OUTPUT_OPTION) $<


##### Executables ##############################################################
ecudump: $(addprefix $(OBJDIR)/,ecudump.o ssm.o logging.o)
	$(CC) $(CFLAGS) -o $(BINDIR)/$@ $^ $(LDLIBS)
ecuscan: $(addprefix $(OBJDIR)/,ecu.o ecuscan.o curses.o logging.o ssm.o libiniparser.a)
	$(CC) $(CFLAGS) -o $(BINDIR)/$@ $^ $(LDLIBS)
simulate_ecuscan: $(addprefix $(OBJDIR)/,ecu.o ecuscan.o curses.o logging.o simulated_ssm.o libiniparser.a)
	$(CC) $(CFLAGS) -o $(BINDIR)/$@ $^ $(LDLIBS)


##### Library submodules #######################################################
$(OBJDIR)/libiniparser.a:
	make --directory libs/iniparser
	cp -f libs/iniparser/libiniparser.* $(OBJDIR)


##### Tests ####################################################################
check_iniparser: $(TESTDIR)/check_iniparser.c $(OBJDIR)/libiniparser.a
	$(CC) $(CFLAGS) -o $(OBJDIR)/$@ $^ $(CHECKLIBS)
	$(OBJDIR)/$@
check_ecu: $(TESTDIR)/check_ecu.c $(OBJDIR)/ecu.o $(OBJDIR)/logging.o $(OBJDIR)/simulated_ssm.o $(OBJDIR)/libiniparser.a
	$(CC) $(CFLAGS) -o $(OBJDIR)/$@ $^ $(CHECKLIBS)
	$(OBJDIR)/$@
check_logging: $(TESTDIR)/check_logging.c $(OBJDIR)/logging.o
	$(CC) $(CFLAGS) -o $(OBJDIR)/$@ $^ $(CHECKLIBS)
	$(OBJDIR)/$@

check: all check_iniparser check_ecu check_logging

