/**
 * check_ecuscan.c - tests ecuscan functionality
 *
 * Copyright by it's authors.
 * Some rights reserved. See COPYING, CREDITS
 */
// #define _GNU_SOURCE // for asprintf
// #include "iniparser.h"
// #include "ecu.h"
// #include "logging.h"
// #include "prefs.h"
#include <check.h>


// Test the loading of ECU parameter data into the ECU data structure for a given ROM ID
START_TEST(test_ecuscan) {
    /*
	user_prefs_type user_prefs = {
	    .log_mode = LOG_MODE_OFF,
	    .log_level = INFO
	};
	init_logs(&user_prefs);

    const _ecu *data = ecu_init("/dev/ttyS0");

    // Assert we loaded known correct values from the SelectMonitor.ini file
    ck_assert_int_eq(data->airFuelCorrection.romAddress, 0x133E);
    ck_assert_int_eq(data->atmosphericPressure.romAddress, 0x0);
    ck_assert_int_eq(data->boostSolenoidDutyCycle.romAddress, 0x0);
    ck_assert_int_eq(data->coolantTemp.romAddress, 0x1337);
    ck_assert_int_eq(data->engineLoad.romAddress, 0x1305);
    ck_assert_int_eq(data->ignitionAdvance.romAddress, 0x1323);
    ck_assert_int_eq(data->injectorPulseWidth.romAddress, 0x1306);
    ck_assert_int_eq(data->isuDutyValve.romAddress, 0x1314);
    ck_assert_int_eq(data->knockCorrection.romAddress, 0x1328);
    ck_assert_int_eq(data->manifoldPressure.romAddress, 0x0);
    ck_assert_int_eq(data->massAirFlow.romAddress, 0x1307);
    ck_assert_int_eq(data->o2Average.romAddress, 0x1310);
    ck_assert_int_eq(data->o2Max.romAddress, 0x0);
    ck_assert_int_eq(data->o2Min.romAddress, 0x0);
    ck_assert_int_eq(data->rpm.romAddress, 0x1338);
    ck_assert_int_eq(data->throttlePosition.romAddress, 0x1329);
    ck_assert_int_eq(data->vehicleSpeed.romAddress, 0x1336);

    ecu_free(data);
    */
}
END_TEST

Suite * test_suite(void) {
    Suite *s;
    TCase *tc_core;

    s = suite_create("ecuscan");

    /* Core test case */
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_ecuscan);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(void) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = test_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
