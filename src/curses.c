/**
 * curses.c - curses-related TUI functionality
 *
 * Copyright 2018 by it's authors.
 * Some rights reserved. See COPYING, CREDITS
 */

#include "logging.h"
#include <ncurses.h>

/*---------------------------------------------------------------------*/
/*  Restores the terminal to its normal state.                         */
/*---------------------------------------------------------------------*/
void cleanup_curses() {
    log_message("Cleaning up curses", DEBUG);
    endwin();
}

/*=================================*/
/* Functions to handle colour text */
/*=================================*/
void setup_colours() {
    start_color();
    init_pair(1,COLOR_WHITE,COLOR_BLACK);
    init_pair(2,COLOR_BLACK,COLOR_GREEN);
    init_pair(3,COLOR_BLACK,COLOR_YELLOW);
    init_pair(4,COLOR_BLACK,COLOR_RED);
    init_pair(5,COLOR_BLACK,COLOR_CYAN);
}

void white_on_black() {
    attron(COLOR_PAIR(1));
}

void black_on_green() {
    attron(COLOR_PAIR(2));
}

void black_on_yellow() {
    attron(COLOR_PAIR(3));
}

void black_on_red() {
    attron(COLOR_PAIR(4));
}

void black_on_cyan() {
    attron(COLOR_PAIR(5));
}

void screen_too_small() {
    move(0, 0);
    white_on_black();
    printw("Please enlarge your terminal: 80x25 minimum required.");
    refresh();
}

/*===============================================*/
/* Draws a bar graph of a given percentage */
/*===============================================*/
void bar(int percent) {
    if (percent > 100) {
        percent = 100;
    }

    black_on_green();
    int x;
    for (x = 0; x < percent / 5; x++) {
        printw(" ");
    }

    black_on_yellow();
    for (int y = x; y < 20; y++) {
        printw(" ");
    }
}

/*===============================================*/
/* Render the current logging mode               */
/*===============================================*/
void print_logmode(log_mode_type logmode, int row, int col) {
    move(row, 0);
    clrtoeol();
    move(row, col);
    switch (logmode) {
        case LOG_MODE_OFF:
            white_on_black();
            printw(" Not Logging ");
            break;
        case LOG_MODE_ON:
            black_on_green();
            printw(" Logging ");
            break;
        case LOG_MODE_ERROR:
            black_on_red();
            printw(" Logging Error ");
            break;
    }
}
