/**
 * logging.c - Data logging functionality.
 *
 * Copyright 2007, 2018 by it's authors.
 * Some rights reserved. See COPYING, CREDITS
 */
#include "ecu.h"
#include "logging.h"
#include "prefs.h"
#include <stdio.h>
#include <time.h>

#define MSG_BUFSIZE 50
static char msg[MSG_BUFSIZE];

FILE *dataFile = NULL,          // ECU data output
     *logFile = NULL;           // application log
user_prefs_type* prefs = NULL;  // User-defined log preferences

void init_logs(user_prefs_type* prefs_in) {
    prefs = prefs_in;
}

void log_ecu(const _ecu *ecu) {
    snprintf(msg, MSG_BUFSIZE, "Logging ECU state");
    log_message(msg, TRACE);
    if (!prefs) {
        fprintf(stderr, "Unable to log ECU data, logging was not correctly initialized\n");
    } else if (prefs->log_mode == LOG_MODE_ON) {
        // Get time in nanoseconds
        struct timespec now;
        clock_gettime(CLOCK_REALTIME, &now);

        // Open the data file if necessary
        if (dataFile == NULL) {
            dataFile = fopen("ecuscan.csv", "a");
            if (dataFile != NULL) {
                // Write out the header
                if (fprintf(dataFile, "time,speed,rpm,tps,maf,batt,temp,o2Avg,o2Min,o2Max,atmosphericPressure,boostSolenoidDutyCycle,engineLoad,ignitionAdvance,injectorPulseWidth,iacDutyCycle,knockCorrection,manifoldPressure\n") < 0) {
                    prefs->log_mode = LOG_MODE_ERROR;
                    return;
                }
            } else {
                prefs->log_mode = LOG_MODE_ERROR;
            }
        }

        // Write the ECU data
        int rc = fprintf(dataFile, "%lld.%.3ld,%d,%d,%d,%d.%02d,%d.%02d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
                    (long long)now.tv_sec, now.tv_nsec / 1000000,
                    prefs->metric_speed ? ecu->vehicleSpeed.data : (ecu->vehicleSpeed.data * 5) / 8,
                    ecu->rpm.data,
                    ecu->throttlePosition.data / 5,
                    ecu->massAirFlow.data / 100, ecu->massAirFlow.data % 100,
                    ecu->batteryVoltage.data / 100, ecu->batteryVoltage.data % 100,
                    prefs->metric_temp ? ecu->coolantTemp.data : (ecu->coolantTemp.data* 9) /5 + 32,
                    ecu->o2Average.data, ecu->o2Min.data, ecu->o2Max.data,
                    ecu->airFuelCorrection.data,
                    ecu->atmosphericPressure.data,
                    ecu->boostSolenoidDutyCycle.data,
                    ecu->engineLoad.data,
                    ecu->ignitionAdvance.data,
                    ecu->injectorPulseWidth.data,
                    ecu->isuDutyValve.data,
                    ecu->knockCorrection.data,
                    ecu->manifoldPressure.data);

        snprintf(msg, MSG_BUFSIZE, "Writing to datafile, return is %d", rc);
        log_message(msg, TRACE);
        if (rc < 0) {
            snprintf(msg, MSG_BUFSIZE, "ERROR Writing ECU data");
            log_message(msg, ERROR);
            prefs->log_mode = LOG_MODE_ERROR;
        } else {
            snprintf(msg, MSG_BUFSIZE, "Wrote %d bytes of ECU data to ecuscan.csv", rc);
            log_message(msg, TRACE);
            fflush(dataFile);
        }
    }

    // Close data file if necessary
    if ((prefs->log_mode == LOG_MODE_OFF) && (dataFile != NULL)) {
        fclose(dataFile);
        dataFile = NULL;
    }
}

void log_message(char* message, log_level_type message_level) {
    if (!prefs) {
        fprintf(stderr, "Unable to log message, logging was not correctly initialized\n");
    } else if (prefs->log_mode == LOG_MODE_ON) {
        // Open the log file if necessary
        if (logFile == NULL) {
            logFile = fopen("ecuscan.log", "a");
            if (logFile == NULL) {
                fprintf(stderr, "Error opening ecuscan.log\n");
                prefs->log_mode = LOG_MODE_ERROR;
                return;
            }
        }

        char *level = NULL;
        if (message_level == TRACE) {
            if (prefs->log_level == TRACE) {
                level = "TRACE";
            }
        } else if (message_level == DEBUG) {
            if (prefs->log_level == DEBUG) {
                level = "DEBUG";
            }
        } else if (message_level == INFO) {
            if (prefs->log_level == DEBUG || prefs->log_level == INFO) {
                level = "INFO";
            }
        } else if (message_level == WARN) {
            if (prefs->log_level == DEBUG || prefs->log_level == INFO || prefs->log_level == WARN) {
                level = "WARN";
            }
        } else if (message_level == ERROR) {
            level = "ERROR";
        }

        if (level) {
            time_t timer;
            char time_str[26];
            struct tm* tm_info;
            time(&timer);
            tm_info = localtime(&timer);
            strftime(time_str, 26, "%Y-%m-%d %H:%M:%S", tm_info);

            // Write the message
            int rc = fprintf(logFile, "%s - %s: %s\n", time_str, level, message);
            if (rc < 0) {
                prefs->log_mode = LOG_MODE_ERROR;
            }
            fflush(logFile);
        }
    }

    // Close log file if necessary
    if (prefs && (prefs->log_mode == LOG_MODE_OFF) && (logFile != NULL)) {
        fclose(logFile);
        logFile = NULL;
    }
}

void close_logs() {
    log_message("Closing log files", DEBUG);
    if (dataFile != NULL) {
        fclose(dataFile);
        dataFile = NULL;
    }
    if (logFile != NULL) {
        fclose(logFile);
        logFile = NULL;
    }
}
