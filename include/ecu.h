/**
 * ecu.h - Definitions for the data structure used to look up and display ECU
 * parameters.
 *
 * Copyright 2019 by it's authors.
 * Some rights reserved. See COPYING, CREDITS
 */
#pragma once

/** Represents a data value the ECU is able to return to us. */
typedef struct ecu_param {
	// The hex address used to query this parameter from the ECU
	char *romAddress;
	// Text description of this ECU parameter
	char *description;
	// The data contained in the ECU parameter
	int data;
} ecu_param;

/** Represents a collection of individual ECU metrics */
typedef struct _ecu {
	// The ECU ROM ID being used
	int romId;
	// The hex representation of the ROM ID used to map ECU addresses
	char *romIdKey;
	// The year/model description corresponding to this ROM ID
	char *modelName;

    ecu_param
        airFuelCorrection,
        atmosphericPressure,
        batteryVoltage,
        boostSolenoidDutyCycle,
        coolantTemp,
        engineLoad,
        ignitionAdvance,
        injectorPulseWidth,
        isuDutyValve,
        knockCorrection,
        manifoldPressure,
        massAirFlow,
        o2Average,
        o2Max,
        o2Min,
        rpm,
        throttlePosition,
        vehicleSpeed;
} _ecu;

/**
 * Loads a map of the memory addresses corresponding to the parameters
 * particular to the ROM version we're connected to (differs by model/year/market).
 *
 * Assumes that ssm_open has been called.
 */
_ecu *load_ecu_settings();

/**
 * Disconnect from the ECU and free allocated resources.
 */
void ecu_free();

/**
 * Takes the integer representation of an ECU ROM ID and parses it into the
 * proper string used by the ROM address mapping table.
 */
char *parseRomIdString(int romId);
