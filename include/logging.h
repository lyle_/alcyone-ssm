/**
 * logging.h - Data logging related declarations
 *
 * Copyright 2007, 2018 by it's authors.
 * Some rights reserved. See COPYING, CREDITS
 */
#pragma once
#include "prefs.h"
#include "ecu.h"

/**
 * Initialize log files according to user preferences.
 *
 * This should be called before any other functions, and should
 * be paired with a call to close_logs() to clean up resources.
 */
void init_logs(user_prefs_type* prefs);

/**
 * Records the given snapshot of ECU data to the data log.
 * Units are translated according to the user's preferences
 * as registered in init_logs().
 */
void log_ecu(const _ecu *ecu);

/**
 * Writes the provided message to the application log
 */
void log_message(char* message, log_level_type level);

/**
 * Close any open log files. Pair with init_logs().
 */
void close_logs();
